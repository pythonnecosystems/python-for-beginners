# 초보자를 위한 Python <sup>[1](#footnote_1)</sup>

Python은 1991년 Guido van Rossum이 만든 프로그래밍 언어로, 널리 사용되는 언어이다.

Python은 Windows, Linux, Mac, Rasberry Pi 등에서 사용할 수 있다.

Python은 구문이 단순하여 다른 언어보다 적은 줄로 개발할 수 있다.

Python은 대부분 인터프리터 언어(interpreted language)로 알려져 있으며 간단히 말해서 Python은 인터프리터 언어라고 할 수 있다. 하지만 백그라운드에서 Python 프로그램은 컴파일 부분이 프로그래머에게 보이지 않는 상태에서 먼저 컴파일된 후 해석된다. 따라서 프로토타이핑이 더 쉽다.

Python은 절차적 방식, 객체 지향 방식 또는 함수적 방식으로 처리할 수 있다.

<a name="footnote_1">1</a>: [Python for Beginners](https://medium.com/@ocrnshn/python-for-beginners-1299a610029f)를 편역한 것이다.
