# 초보자를 위한 Python

## 들여쓰기(Indentation)
Python에서 들여쓰기의 주요 목적은 루프, 조건문, 함수 및 클래스 내의 문과 같은 문의 범위를 정의하는 것이다. 일관되고 적절한 들여쓰기는 인터프리터가 코드의 논리적 구조를 이해하는 데 매우 중요하다. 들여쓰기는 단순히 Python의 스타일이나 규칙의 문제가 아니다.

## 변수(Variables)
기본 데이터 타입은 숫자, 문자열, 부울, 시퀀스와 사전이다.

```python
myint = 5
myfloat = 13.2
mystr = "This is a string"
mybool = True
mylist = [0, 1, "two", 3.2, False]
mytuple = (0, 1, 2)
mydict = {"one" : 1, "two" : 2}

myint = "Hello" # allows re-declare a variable in different type
```

### 시퀀스 작업

**리스트(List)**:

```python
L = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
print(mylist[2])      # access an element
# Prints 'c'
print(mylist[2:7:2])  # slice with step [start:stop:step]
# Prints ['c', 'e', 'g']
print(mylist[::-1])   # revert the list
# Prints ['i', 'h', 'g', 'f', 'e', 'd', 'c', 'b', 'a']
```

![](./images/1_VoULH2_It4FO2EaD46xdGg.webp)

**튜플과 딕셔너리**:

```python
mytuple = (0, 1, 2)
mydict = {"one" : 1, "two" : 2}
print(mytuple[1])
print(mydict["one"])
```

**지역과 전역 변수**

```python
mystr = "a string"        # global variable

def someFunction():
    mystr = "def"         # local variable
    print (mystr)         # prints local variable
    #prints 'def'

someFunction()
print (mystr)             # prints global variable
#prints 'a string'


del mystr                 # deletes global variable
print (mystr)             # NameError
```

## 함수(Functions)

```python
def power(num, x=1):
    result = 1
    for i in range(x):
        result = result * num
    return result

def multi_add(some_txt, *args):
    print(some_txt)
    result = 0
    for x in args:
        result = result + x
    return result


print(power(2))
print(power(2, 3))
print(power(x=3, num=2))
print(multi_add("hi", 4, 5, 10, 4))
```

- `x=1`과 같이 인수의 기본값을 설정할 수 있다.
- `power(x=3, num=2)`와 같이 인자 이름을 지정하여 어떤 순서로든 함수에 인자를 지정할 수 있다.
- 인수의 개수가 가변적인 함수를 정의할 수 있지만(`def multi_add(*args)`), 반드시 함수의 마지막 인수가 되어야 한다.

## 참과 거짓 평가
해당 클래스가 거짓을 반환하는 Bool 메서드를 정의하거나 길이가 0을 반환하는 len 메서드를 정의하지 않는 한 모든 객체는 부울 참과 동등한 것으로 간주된다.

거짓 값:

- `False`, `None`
- `0`, `0.0`, `Decimal(0)`, `Fraction(0,x)`
- 빈 문자열, 빈 컬렉션: `''`, `()`, `[]`, `{}`
- 빈 집합, 빈 범위: `set()`, `range(0)`
- `bool`과 `len` 함수를 `False`로 정의하는 클래스:

```python
class myclass:
  def __bool__(self):
    return False
  def __len__(self):
    return 0
```

## 조건문(Conditionals)

```python
def compare(x, y):
    if x < y:
        result = "x is less than y"
    elif x == y:
        result = "x is equal to y"
    else:
        result = "x is greater than y"
    print(result)

def is_greater(x,y):
    result = False if (x =< y) else True
    return resut

def between(x, y, z):
  if not y or not z:
    return "X or Y is not valid."
  if (x < y and x > z) or (x > y and x < z):
    return "x is between y and z"

def access(user):
   match user:
      case "admin" | "manager": return "Full access"
      case "Guest": return "Limited access"
      case _: return "No access"
```

- 조건문: `if`, `elif`, `else`
- 또는 간략하게 "`a if C else b`"
- Python 3.0에서 `match-case`문 도입
- 연산자: `and`, `or`, `not`

## 루프(Loops)

```python
x=1
while (x < 5):
  print(x)
  x = x + 1
#prints 1, 2, 3, 4

for x in range(1, 5):
    print (x)
#prints 1, 2, 3, 4
      
int_arr= [1, 2, 3, 4, 5]
for i in int_arr:
    print (i)
#prints 1, 2, 3, 4, 5
    
for x in range(5,10):
    if (x == 8): break
        #if (x % 2 == 0): continue
    print (x)
#prints 5, 6, 7

for x in range(5,10):
    if (x % 2 == 0): continue
    print (x)
#prints 5, 6, 7, 8, 9
    
str_arr = ["one","two","three"]
for i, d in enumerate(str_arr):
    print (i, d)
#prints 0 one, 1 two, 2 three
```

- 루프를 위하여 `while` 또는 `for`를 사용할 수 있다.
- 내장된 `range` 함수는 시작과 끝을 지정하여 일련의 숫자를 가져오는 데 사용할 수 있다. `range(start, end)`에서 `start` 값은 포함되지만 `end` 값은 제외된다.
- 내장된 `enumerate` 함수는 요소와 인덱스를 함께 가져오는 데 사용할 수 있다.

## 문자열 포맷팅

```python
str1 = "Hello {0}, I am {1}".format("world", "Jane Doe")

tmpl = Template("Hello ${planet}, I am ${name}")
str2 = tmpl.substitute(planet="Mars", author="Joe Marini")

args = { 
  "author": "Joe Marini",
  "title": "Advanced Python"
}
str3 = tmpl.substitute(args) 
```

- `String.format()`을 사용할 수 있다.
- `key=value` 할당 또는 딕셔너리로 템플릿을 사용할 수 있다.

## 클래스

```python
class Vehicle():
    def __init__(self, color):
        self.color= color
        self.speed = 0

    def drive(self, speed):
        self.speed = speed

    def details(self):
        print("Color:", self.color)

class Car(Vehicle):
    def __init__(self, engine, color):
        super().__init__(color)
        self.wheels = 4
        self.doors = 4
        self.engine = engine

    def drive(self, speed):
        super().drive(speed)
        print("Driving car at", self.speed, "km/h")

    def details(self):
        print(self.color, self.engine, "car with", self.wheels, "wheels and", self.doors, "doors")

class Motorcycle(Vehicle):
    def __init__(self, sidecar, color):
        super().__init__(color)
        self.sidecar = sidecar
        if sidecar:
            self.wheels = 2
        else:
            self.wheels = 3
        self.doors = 0

    def drive(self, speed):
        super().drive(speed)
        print("Driving motorcycle at", self.speed, "km/h")

    def details(self):
        print(self.color, "motorcycle with", self.wheels, "wheels",
              "having a sidecar" if self.sidecar else "")

if __name__ == '__main__':
    car1 = Car("gas", "gray")
    car2 = Car("electric", "blue")
    mc1 = Motorcycle(True, "red")

    mc1.details()
    car1.details()
    car2.details()

    car1.drive(70)
    car2.drive(90)
    mc1.drive(50)
```

- 클래스 타입을 사용하여 클래스를 정의하고 상속을 위해 괄호 안에 부모 클래스를 넣는다. 예 `class Motorcycle(Vehicle)`
- 초기화 함수 안에 생성기를 정의합니다. 예: `def __init__(self, arg1, arg2):`
- 각 클래스 함수의 첫 번째 매개변수로 `self` 매개변수를 사용한다. 예: `def drive(self, speed)`
- `self` 키워드를 사용하여 Python에서 정의된 클래스의 변수, 속성 및 메서드fmf를 액세스할 수 있다. 예: `self.drive()`, `self.color`

## 모듈 사용하기
- `imort` 키워드를 사용하여 내장 모듈 또는 커스텀 모듈을 가져올 수 있다.
- `as`를 사용하여 모듈의 이름을 바꿀 수 있다.

다음을 포함하는 `mymodule.py` 파일을 만들었다고 가정하자.

```python
def greeting(name):
    print("Hello, " + name)
```

다음과 같이 다른 모듈에서 함수를 사용할 수 있다.

```python
import mymodule

mymodule.greeting("Jonathan")
```

내장 모듈을 임포트한다.

```python
import math

print("Square root of 81:", math.sqrt(81))
print("Pi number:", math.pi)
```

모듈 이름을 변경할 수 있다.

```python
import mymodule as mm

a = mm.greeting("Jessica")
```

모듈로 부터 임포트할 수 있다.

```python
from mymodule import greeting

greeting("John")
```

## 예외(Exceptions)
예외는 `try`, `except`, `finally` 블록으로 처리된다. 여러 `except`를 사용하여 다양한 종류의 예외를 처리할 수 있다.

키워드로 예외의 세부 사항을 설명할 수 있다. 예: `except ZeroDivisionError as e`

`finally` 블록은 예외가 있든 없든 항상 수행한다.

```python
def divide(x, y):
    try:
        print(x / y)
    except ZeroDivisionError as e:
        print("Division by zero!")
    except TypeError as e:
        print("Invalid dividend or divisor!")
        print(e)
    finally:
        print("Finally print")

if __name__ == '__main__':
    divide(10, 0)
    divide(10, "a")
    divide("b", 10)
    divide(10, 2)
```

출력:

```
Division by zero!
Finally print
Invalid dividend or divisor!
unsupported operand type(s) for /: 'int' and 'str'
Finally print
Invalid dividend or divisor!
unsupported operand type(s) for /: 'str' and 'int'
Finally print
5.0
Finally print
```

## 파일

```python
#write a file
f = open("test.txt","w+")
for i in range(10):
   f.write("This is line %d\r\n" % (i+1))
f.close()
    
#read whole file at once
f = open("test.txt","r")
if f.mode == 'r': 
      contents = f.read()
       print (contents)

#read file line by line
f = open("test.txt","r")
if f.mode == 'r':      
    fl = f.readlines() # readlines reads the individual lines into a list
    for l in fl:
        print (l)
```

- 먼저 필요한 권한으로 파일을 연다.

| Character | 의미 |
|-----------|-----|
| `r` | 파일을 읽기 위한 열기 |
| `w` | 파일에 쓰기 위한 열기, 먼저 파일을 비운다 |
| `x` | 파일을 생성하기 위한 열기, 파일이 이미 있다면 실패한다 |
| `a` | 파일을 쓰기 위한 열기, 파일이 있다면 파일 끝에 추가한다 |
| `b` | 이진 모드 |
| `t` | 텍스트 모드 (default)|
| `+` | 파일의 업데이트를 위한 열기 (읽기와 쓰기) |

- `read()`를 사용하여 문서 전체를 읽거나 `readline()`을 사용하여 한 줄씩 읽을 수 있다.
- `write()`를 사용하여 한 줄을 쓴다.
- 작업 후 파일을 닫는다 (읽기 작업에는 필요하지 않음).

## `os` util

```python
path.exists("test.txt") # checks if path exists in current directory
path.isfile("test.txt") # checks if path points out a file
path.isdir("test.txt")  # checks if path points out a directory
path.realpath("test.txt") # returns absolute path
path.split("/home/test/fold/test.txt") 
#returns a tuple of path and file name ('/home/test/fold', 'test.txt')
```

## Shell Util

```python
import shutil
from shutil import make_archive

src = path.realpath("test.txt");
dst = "new_" + src
shutil.copy(src,dst)
        
root_dir,tail = path.split(src)
shutil.make_archive("archive", "zip", root_dir)
```

## 내장 함수
https://docs.python.org/3/library/functions.html 에는 여러 가지 유용한 내장 함수가 나열되어 있다.

그중 몇 가지는 다음과 같다. `any`, `all`, `min`, `max`, `sum`, `filter`, `map`, `sort`

```python
mylist = [1, 2, 3, 4, 5, 6]
    
any(mylist) # return true if any of the list values are true - True
all(mylist) # return true if all of the list values are true - True
min(mylist) # return minimum value of the list - 1
max(mylist) # return maximum value of the list - 6
sum(mylist) # return sum of the values of the list - 21
    
def oddFilterFunc(x):
    return not x % 2 == 0
odds = list(filter(oddFilterFunc, mylist)) 
# applies the function to all elements of the list and 
# filters values for which function returns True

def squareFunc(x):
    return x**2
squares = list(map(squareFunc, mylist))
# applies the function to all elements of the list and 
# returns the list of new values
```

## 이터레이터(Iterators)

```python
# define a list of grades and students in order
grades = [95, 60, 70, 92, 68]
students = ["Jane", "Joe", "Daryl", "Rick", "Sam"]


i = iter(students) # create iterator
print(next(i))    # Jane
print(next(i))    # Joe
print(next(i))    # Daryl

# print out grades and student together
for grade in zip(students, grades):
    print(grade[0] + " - " + m[1])

# print out grades and student with index
for i, m in enumerate(zip(students, grades), start=1):
    print(i, m[0], " - ", m[1])
```
